package com.example.belajarmvvmjava;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.belajarmvvmjava.adapter.UserAdapter;
import com.example.belajarmvvmjava.model.UserModel;
import com.example.belajarmvvmjava.viewModel.UserViewModel;
import com.example.belajarmvvmjava.viewModel.UserViewModelFactory;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    RecyclerView listSelected;

    private UserViewModel userViewModel;
    private UserAdapter userAdapter;

    ArrayList<UserModel> userModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        userModels = getIntent().getExtras().getParcelableArrayList("selected");

        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        userAdapter = new UserAdapter(userModels);

        listSelected = findViewById(R.id.listUserSelect);

        listSelected.setAdapter(userAdapter);
        listSelected.setLayoutManager(new LinearLayoutManager(this));

        userViewModel.getUserModelMutableLiveData().observe(this, userModel -> {
            //userAdapter.submitList(userModel);
            userModels.clear();
            userModels.addAll(userModel);
            userAdapter.notifyDataSetChanged();
        });
    }
}