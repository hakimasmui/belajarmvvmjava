package com.example.belajarmvvmjava.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.belajarmvvmjava.R;
import com.example.belajarmvvmjava.model.UserModel;
import com.example.belajarmvvmjava.viewModel.UserViewModel;

public class DialogTambahUser extends DialogFragment {

    private UserViewModel userViewModel;

    EditText edtEmail;
    EditText edtPassword;
    Button btnLogin;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_add_user, container, false);

        edtEmail = view.findViewById(R.id.edtEmail);
        edtPassword = view.findViewById(R.id.edtPassword);
        btnLogin = view.findViewById(R.id.btnLogin);

        edtEmail.setText(userViewModel.getUserSelected());

        btnLogin.setOnClickListener(v -> {
            if (TextUtils.isEmpty(edtEmail.getText()) || TextUtils.isEmpty(edtPassword.getText())) {
                Toast.makeText(getContext(),
                        "Email dan Password harus diisi",
                        Toast.LENGTH_SHORT).show();
            } else if (!Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText()).matches()) {
                Toast.makeText(getContext(),
                        "Email invalied",
                        Toast.LENGTH_SHORT).show();
            } else {
                UserModel userModel = new UserModel(edtEmail.getText().toString(),
                        edtPassword.getText().toString(), false);

                userViewModel.addUser(userModel);
            }
        });

        return view;
    }
}
