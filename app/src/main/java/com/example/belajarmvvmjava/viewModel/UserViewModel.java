package com.example.belajarmvvmjava.viewModel;

import android.app.Application;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.example.belajarmvvmjava.model.UserModel;
import com.example.belajarmvvmjava.repository.UserLocalData;
import com.example.belajarmvvmjava.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UserViewModel extends AndroidViewModel {

    private final UserRepository userRepository;

    private ArrayList<UserModel> userModels = new ArrayList<>();
    private MutableLiveData<ArrayList<UserModel>> userMutableLiveData;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
    }

    public LiveData<ArrayList<UserModel>> getUserModelMutableLiveData() {
        if (userMutableLiveData == null) {
            userMutableLiveData = new MutableLiveData<>();
            userModels = userRepository.getUsers();
            userMutableLiveData.setValue(userModels);
        }
        return userMutableLiveData;
    }

    public void addUser(UserModel user) {
        userRepository.insertUser(user);

        userModels.add(user);
        userMutableLiveData.postValue(userModels);
    }

    public void editItem(int position, boolean isSelected) {
        UserModel userModel = Objects.requireNonNull(userMutableLiveData.getValue()).get(position);
        userModel.setSelected(isSelected);
        userModels.set(position, userModel);
        userMutableLiveData.postValue(userModels);
    }

    public String getUserSelected() {
        ArrayList<UserModel> userAll = userMutableLiveData.getValue();
        ArrayList<UserModel> userSelected = new ArrayList<>();
        if (userAll != null) {
            for (int i = 0; i < userAll.size(); i++) {
                UserModel userModel = userAll.get(i);
                if (userModel.isSelected()) {
                    userSelected.add(userModel);
                }
            }
        }
        if (userSelected.size() == 0)
            return "";
        else
            return userSelected.get(0).getEmail();
    }

    public void setUserSelected(ArrayList<UserModel> users) {
        userMutableLiveData.postValue(users);
    }

    public ArrayList<UserModel> getUserModelSelected() {
        return userMutableLiveData.getValue();
    }
}
