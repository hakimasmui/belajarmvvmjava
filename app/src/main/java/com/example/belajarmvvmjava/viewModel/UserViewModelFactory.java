package com.example.belajarmvvmjava.viewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.belajarmvvmjava.model.UserModel;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class UserViewModelFactory implements ViewModelProvider.Factory {

    private ArrayList<UserModel> userModels;

    public UserViewModelFactory(ArrayList<UserModel> userModels) {
        this.userModels = userModels;
    }

    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> aClass) {
        try {
            return aClass.getConstructor(UserViewModel.class, int.class)
                    .newInstance(userModels);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }
}
