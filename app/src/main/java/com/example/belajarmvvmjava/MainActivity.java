package com.example.belajarmvvmjava;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.belajarmvvmjava.adapter.UserAdapter;
import com.example.belajarmvvmjava.dialog.DialogTambahUser;
import com.example.belajarmvvmjava.model.UserModel;
import com.example.belajarmvvmjava.viewModel.UserViewModel;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    Button btnCek;
    Button btnTambah;
    RecyclerView listUser;

    private UserViewModel userViewModel;
    private UserAdapter userAdapter;

    ArrayList<UserModel> userModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        userAdapter = new UserAdapter(userModels);

        btnCek = findViewById(R.id.btnCek);
        btnTambah = findViewById(R.id.btnTambah);
        listUser = findViewById(R.id.listUser);

        listUser.setAdapter(userAdapter);
        listUser.setLayoutManager(new LinearLayoutManager(this));

        userViewModel.getUserModelMutableLiveData().observe(this, userModel -> {
            userModels.clear();
            userModels.addAll(userModel);
            userAdapter.notifyDataSetChanged();
        });

        userAdapter.setmListener((position, isSelected) -> {
            userViewModel.editItem(position, isSelected);
        });

        btnCek.setOnClickListener(v -> {
            Intent i = new Intent(this, RegisterActivity.class);
            i.putParcelableArrayListExtra("selected", userViewModel.getUserModelSelected());
            startActivity(i);
        });

        btnTambah.setOnClickListener(v -> {
            /*AlertDialog.Builder dialognya = new AlertDialog.Builder(MainActivity.this);
            AlertDialog alert = dialognya.create();
            LayoutInflater li = LayoutInflater.from(getApplicationContext());
            View view = li.inflate(R.layout.dialog_add_user, null);

            alert.setView(view);
            alert.show();*/

            DialogTambahUser dialogTambahUser = new DialogTambahUser();
            dialogTambahUser.show(getSupportFragmentManager(), "Add User");
        });
    }
}