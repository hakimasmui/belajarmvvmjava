package com.example.belajarmvvmjava.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.belajarmvvmjava.R;
import com.example.belajarmvvmjava.model.UserModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private ArrayList<UserModel> userModels = new ArrayList<>();

    private ItemClickListener mListener;

    public UserAdapter(ArrayList<UserModel> userModels) {
        this.userModels = userModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserModel userModel = userModels.get(position);

        holder.tvEmail.setText(userModel.getEmail());
        holder.cbSelect.setChecked(userModel.isSelected());

        holder.cbSelect.setOnCheckedChangeListener(null);

        holder.cbSelect.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (mListener != null)
                mListener.onCheckedItem(position, isChecked);
        });

        holder.itemView.setOnClickListener(v -> {
            if (mListener != null)
                mListener.onCheckedItem(position, !userModel.isSelected());
        });
    }

    @Override
    public int getItemCount() {
        return userModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvEmail;
        CheckBox cbSelect;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvEmail = itemView.findViewById(R.id.tvEmail);
            cbSelect = itemView.findViewById(R.id.cbSelect);
        }
    }

    public void setmListener(ItemClickListener mListener) {
        this.mListener = mListener;
    }

    public interface ItemClickListener {
        void onCheckedItem(int position, boolean isSelected);
    }
}

/*
public class UserAdapter extends ListAdapter<UserModel, UserAdapter.ViewHolder> {

    private ItemClickListener mListener;

    public UserAdapter() {
        super(diff_callback);
    }

    @NonNull
    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.adapter_user,
                parent,
                false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.ViewHolder holder, int position) {
        UserModel userModel = getItem(position);
        holder.tvEmail.setText(userModel.getEmail());
        holder.cbSelect.setChecked(userModel.isSelected());

        holder.cbSelect.setOnCheckedChangeListener(null);

        holder.cbSelect.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (mListener != null)
                mListener.onCheckedItem(position, isChecked);
        });

        holder.itemView.setOnClickListener(v -> {
            if (mListener != null)
                mListener.onCheckedItem(position, !userModel.isSelected());
        });
    }

    public static final DiffUtil.ItemCallback<UserModel> diff_callback = new DiffUtil.ItemCallback<UserModel>() {
        @Override
        public boolean areItemsTheSame(@NonNull UserModel oldItem, @NonNull UserModel newItem) {
            return Objects.equals(oldItem.getEmail(), newItem.getEmail());
        }

        @Override
        public boolean areContentsTheSame(@NonNull UserModel oldItem, @NonNull UserModel newItem) {
            return oldItem.getEmail().equals(newItem.getEmail()) &&
                    oldItem.isSelected() == newItem.isSelected();
        }
    };

    public void setmListener(ItemClickListener mListener) {
        this.mListener = mListener;
    }

    public interface ItemClickListener {
        void onCheckedItem(int position, boolean isSelected);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvEmail;
        CheckBox cbSelect;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvEmail = itemView.findViewById(R.id.tvEmail);
            cbSelect = itemView.findViewById(R.id.cbSelect);
        }
    }
}*/
