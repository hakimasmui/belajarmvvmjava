package com.example.belajarmvvmjava.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.belajarmvvmjava.model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    UserLocalData userLocalData;

    public UserRepository(Application application) {
        userLocalData = new UserLocalData(application);
    }

    public ArrayList<UserModel> getUsers() {
        return userLocalData.getUserModelLiveData();
    }

    public void insertUser(UserModel user) {
        userLocalData.insertUser(user);
    }
}
