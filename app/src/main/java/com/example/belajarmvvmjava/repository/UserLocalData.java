package com.example.belajarmvvmjava.repository;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.belajarmvvmjava.database.DBHelper;
import com.example.belajarmvvmjava.model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class UserLocalData {
    private final DBHelper dbHelper;

    public UserLocalData(Application application) {
        dbHelper = new DBHelper(application);
    }

    public ArrayList<UserModel>getUserModelLiveData() {
        ArrayList<UserModel> userModels = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cur = db.rawQuery("SELECT * FROM users", null);

        if (cur.getCount() > 0) {

            cur.moveToFirst();
            for (int i = 0; i < cur.getCount(); i++) {
                cur.moveToPosition(i);

                UserModel userModel = new UserModel(
                        cur.getString(cur.getColumnIndexOrThrow("email")),
                        cur.getString(cur.getColumnIndexOrThrow("password")),
                        false
                );
                userModels.add(userModel);
            }
        }
        return userModels;
    }

    public void insertUser(UserModel user) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues data = new ContentValues();
        data.put("email", user.getEmail());
        data.put("password", user.getPassword());

        db.insert("users", null, data);
    }
}
